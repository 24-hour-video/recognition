const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const sns = new AWS.SNS({apiVersion: '2010-03-31'})

class SNS {
  constructor (body, topic) {
    // should not empty
    this.body = body
    this.topic = topic
  }

  send() {
    const params = {
      Message: JSON.stringify(this.body), /* required */
      TopicArn: this.topic,
      MessageStructure: 'json'
    };

    return sns.publish(params).promise();
  }
}

module.exports = SNS