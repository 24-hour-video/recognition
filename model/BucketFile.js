class BucketFile {
    static fromBucketSQSEvent(event) {
        const file = new BucketFile()
        file.bucketSQSEvent = event
        file.unpack()

        return file;
    }

    constructor() {
        this.bucketSQSEvent = null
        this.bucket = null
        this.fileName = null
    }

    unpack() {
      const record = JSON.parse(this.bucketSQSEvent.Records[0].body)
      const fileInfos = record.Records[0].s3
      this.bucket = fileInfos.bucket.name
      this.fileName = fileInfos.object.key
    }

}

module.exports = BucketFile