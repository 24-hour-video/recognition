const Message = require('../model/Message')

class LabelingBody {
  constructor (labels, bucket, key) {
    this.labels = labels
    this.content = {}
    this.content.default = {
      labels,
      bucket,
      key
    }
  }

}

module.exports = LabelingBody