const SNS = require('./MessageStrategy/SNS')

class Message {
  static SNS(body, topic) {
    return new Message(new SNS(body, topic))
  }

  constructor (strategy) {
    if(typeof strategy.send !== 'function') {
      throw new Error('Message strategy should have a send() method')
    }
    this.strategy = strategy
  }

  send() {
    return this.strategy.send()
  }
}

module.exports = Message