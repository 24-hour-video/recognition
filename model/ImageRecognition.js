const AWS = require('aws-sdk')
const rekognition = new AWS.Rekognition({apiVersion: '2016-06-27', region: process.env.REGION || 'eu-west-1'})

class ImageRecognition {
  constructor (bucket, fileName) {
    this.bucket = bucket
    this.fileName = fileName
    this._params = {}
  }

  get labels() {
    this._params.detectLabels = {
      Image: {
        S3Object: {
          Bucket: this.bucket, // fileInfos.bucket.name,
          Name: this.fileName, // fileInfos.object.key,
        }
      },
      MaxLabels: 123,
      MinConfidence: 70
    };

    const labels = rekognition.detectLabels(this._params.detectLabels).promise()
    return labels;
  }
}

module.exports = ImageRecognition