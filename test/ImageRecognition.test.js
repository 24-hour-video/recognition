const chai = require('chai')
const sinon = require('sinon')
const rewire = require('rewire')
const expect = chai.expect

const mockedBucketEventResponse = (bucket, filename) => {
  const mockedBucketEvent = {
    "Records": [
      {
        "messageId": "efb3fc62-67e6-4068-a493-9de091fab914",
        "receiptHandle": "AQEBRNORv6r2F5iZLMEK1oUZu44el0Z/MCYJ2NFT0+QPylTbuBwoY5ShI/bONE+HbuSXGEaxDkUNNZKbjqLJ1Tk71rIDgUDgViYGx5apWQXrXhePGekVDMtyrca0RPi+gGVl0MRi4hJmIh7TjHd0rGsiFAUmJ6BwfzBc54cmPm0lN75erAtLB9j7VsSkggOINhvpZ06asmaZzDkgfkiwqXWqJJKbbmAkgs9FCBLB13ZDGmg8osIQfsggsixyZ2/q+cnftOG5gJl5sMqAZaD3YSo0T+a66a9jQsoYqv+kBTzF/q+1fdfn18uljFZwvAWJwf/SWgnq310fI0u361rZRC/ArtcV5Uwfp2yi9fOGy7nHSCQlvF3ngeOVdJWicQdtwsqrIwxOYQkx4ITIPD28oFOWZQ==",
        "body": "{\"Records\":[{\"eventVersion\":\"2.1\",\"eventSource\":\"aws:s3\",\"awsRegion\":\"eu-central-1\",\"eventTime\":\"2018-12-06T13:59:51.793Z\",\"eventName\":\"ObjectCreated:Put\",\"userIdentity\":{\"principalId\":\"A363Q0JTGK5D8F\"},\"requestParameters\":{\"sourceIPAddress\":\"78.131.57.216\"},\"responseElements\":{\"x-amz-request-id\":\"341ECF448DC291D5\",\"x-amz-id-2\":\"oLgamxbdN2iU/ZvKtzHlwz3UcWcl29BoFpenPIb9oNVyMGwaHAfaAncqrgScI9L55zcby4MttfU=\"},\"s3\":{\"s3SchemaVersion\":\"1.0\",\"configurationId\":\"24-hours-video-raw\",\"bucket\":{\"name\":\"balint-sera-24h-videos-uploads\",\"ownerIdentity\":{\"principalId\":\"A363Q0JTGK5D8F\"},\"arn\":\"arn:aws:s3:::" + bucket + "\"},\"object\":{\"key\":\"" +filename+ "\",\"size\":6087209,\"eTag\":\"437604badbc554797f6e454dd8a769d4\",\"sequencer\":\"005C092B579ED02DF5\"}}}]}",
        "attributes": [
          null
        ],
        "messageAttributes": {},
        "md5OfBody": "415800e053e9e8d6b4cf01dc2ba3a0cd",
        "eventSource": "aws:sqs",
        "eventSourceARN": "arn:aws:sqs:eu-central-1:849529972347:24-hour-videos",
        "awsRegion": "eu-central-1"
      }
    ]
  }
  return mockedBucketEvent
}
const mockedDetectLabelsResponse = [
  {
    Name: 'Clothing',
    Confidence: 99.70092010498047,
    Instances: [],
    Parents: []
  },
  {
    Name: 'Footwear',
    Confidence: 99.70092010498047,
    Instances: [],
    Parents: [Array]
  },
  {
    Name: 'Apparel',
    Confidence: 99.70092010498047,
    Instances: [],
    Parents: []
  }]

describe('ImageRecognition module can send the params to Rekognition', () => {
  describe('when there is an event', () => {
    let ImageRecognition
    beforeEach(() => {
      ImageRecognition = rewire('../model/ImageRecognition')
    })
    it('should get the label information (mocked)', async () => {
      ImageRecognition.__set__({
        'rekognition': {
          detectLabels: (params) => {
            return {
              promise: () => {
                return Promise.resolve(mockedDetectLabelsResponse)
              }
            }
          }
        }
      })
      const fileName = 'example.jpg';
      const bucketName = 'balint-sera-24h-videos-uploads';
      const imgRec = new ImageRecognition(bucketName, fileName)
      const labels = await imgRec.labels;

      expect(labels.length).to.equal(3)
      expect(imgRec._params.detectLabels.Image.S3Object.Bucket).to.equal(bucketName)
      expect(imgRec._params.detectLabels.Image.S3Object.Name).to.equal(fileName)

    })
  })
})