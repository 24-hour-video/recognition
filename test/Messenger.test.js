const chai = require('chai')
const sinon = require('sinon')
const rewire = require('rewire')
const expect = chai.expect

describe('Message', () => {
  describe('when initialized with the sns factory method', () => {
    let Message
    beforeEach(() => {
      Message = rewire('../model/Message')
    })
    it('should be sent to SNS', async () => {
      const patched = require('../model/MessageStrategy/SNS')
      patched.prototype.send = () => {
        return Promise.resolve({error: 0})
      }

      Message.__set__('SNS', patched)
      const topic = 'SNS-topic-name';

      const message = Message.SNS("test message", topic)
      const result = await message.send()

      expect(result.error).to.equal(0)
    })

    it.skip('should throw error when the strategy is missing', () =>{
      // @TODO implement this
    })
  })
})