'use strict';
const headers = {
  'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
  'Access-Control-Allow-Methods': 'GET,OPTIONS',
  'Access-Control-Allow-Origin': '*'
}

const ImageRecognition = require('./model/ImageRecognition')
const BucketFile = require('./model/BucketFile')
const LabelingBody = require('./model/LabelingBody')
const Message = require('./model/Message')

exports.handler = async (event, context) => {
  try  {
    const file = BucketFile.fromBucketSQSEvent(event)
    const imgRec = new ImageRecognition(file.bucket, file.fileName)
    const labels = await imgRec.labels;
    console.log("rekogntion labels", labels)
    const body = new LabelingBody(labels, file.bucket, file.fileName)
    const message = Message.SNS(body.content, process.env.SNS_RES_TOPIC)
    const result = await message.send()
    console.log("sns result", result);
    return {
      statusCode: 200,
      body: labels,
      headers
    }
  } catch(err) {
    console.log("rekogntion errors", err)
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify(err)
    }
  }
}
